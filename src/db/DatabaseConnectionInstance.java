/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import javax.swing.*;
import java.sql.*;

/**
 *
 * @author MUSTAFA-OCMJD
 */
public class DatabaseConnectionInstance {
    
    private String dbName;
    private String dbHost;
    private Connection connection;
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String USER = "root";
    private static final String PASSWORD = "";
    
    private boolean isConnected = false;
    
    public DatabaseConnectionInstance(String h, String db){
        this.dbName = db;
        this.dbHost = h;
        this.init();
    }
    
    private void init(){
        try{
            Class.forName(DRIVER);
            String dbUrl = "jdbc:mysql://"+this.dbHost+"/"+this.dbName+"?user="+USER+"&password="+PASSWORD+"&autoDeserialize=true";
            connection = DriverManager.getConnection(dbUrl);
            this.isConnected = true;
        }
        catch(Exception e){
            JOptionPane.showMessageDialog(null, "Error at Loading MySQL Driver" + e.getMessage(), "Cannot Connect", JOptionPane.ERROR_MESSAGE);
        }        
    }
    
    public boolean isConnected(){
        return this.isConnected;
    }
    
    public Connection getConnection(){
        if(this.isConnected){
            return this.connection;
        }
        else{
            return null;
        }
    }
}
