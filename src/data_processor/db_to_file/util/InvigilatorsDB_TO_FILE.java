/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data_processor.db_to_file.util;

import data_processor.Invigilator_Category;
import java.sql.*;
import java.io.*;
import java.util.*;
/**
 *
 * @author MUSTAFA-OCMJD
 */
public class InvigilatorsDB_TO_FILE {
    
    private Connection connection;
    private Statement statement;
    private File file;
    
    private List<InvigilatorsDB_TO_FILE.InvigilatorRow> rowsOfInvigilators = new ArrayList<>();
    
    public InvigilatorsDB_TO_FILE(Connection c, File f) throws SQLException, IOException{
        this.connection = c;
        this.file = f;
        this.init();
        this.writeToFile();
        //System.out.println(this.rowsOfInvigilators.toString());
    }
    
    private void writeToFile() throws IOException{
        StringBuilder build = new StringBuilder();
        for(InvigilatorsDB_TO_FILE.InvigilatorRow row: this.rowsOfInvigilators){
            build.append(row.getLine());
            build.append("\n");
        }
        build.append("-->END<--");
        try (FileWriter writerObj = new FileWriter(this.file)) {
            writerObj.write(build.toString());
            writerObj.flush();
        }
    }
    
    private void init() throws SQLException{
        this.statement = this.connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet result = this.statement.executeQuery("SELECT * FROM staffs INNER JOIN departments ON "
                + "staffs.DepartmentID=departments.DepartmentID INNER JOIN designations ON staffs.DesignationID=designations.DesignationID");
        while(result.next()){
            Invigilator_Category categ = Invigilator_Category.INVIGILATOR;
            int supervisors[] = {4, 5, 2}; //array of those senior staffs
            int destig = result.getInt("designations.DesignationID");
            for(int x:supervisors){
                if(x == destig){
                    categ = Invigilator_Category.SUPERVISOR;
                    break;
                }
            }
            this.rowsOfInvigilators.add(new InvigilatorRow(result.getString("staffs.FullName"), result.getString("departments.DepartmentName"),
                    categ)); //populate list of invigilators.
        }
    }
    
    private class InvigilatorRow{
        
        private String name;
        private String department;
        private Invigilator_Category category;
        
        public InvigilatorRow(String n, String d, Invigilator_Category c){
            this.name = n;
            this.department = d;
            this.category = c;
        }

        /**
         * @return the name
         */
        public String getName() {
            return name;
        }

        /**
         * @return the department
         */
        public String getDepartment() {
            return department;
        }

        /**
         * @return the category
         */
        public Invigilator_Category getCategory() {
            return category;
        }

        public String getLine(){
            return this.name+"|"+this.category.toString()+"|"+this.department;
        }
        
        @Override
        public String toString() {
            return "InvigilatorRow{" + "name=" + name + ", department=" + department + ", category=" + category + '}'+"\n";
        }        
        
        
    }
}
