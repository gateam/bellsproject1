/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data_processor.db_to_file.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 *
 * @author MUSTAFA-OCMJD
 */
public class CoursesDB_TO_FILE {

    private Connection connection;
    private Statement statement;
    private File file;
    private int []levels = {100, 200, 300, 400, 500};
    private Map<String, CoursesDB_TO_FILE.CourseRow> rowsOfStudents = new HashMap<>();
    private Map<String, CoursesDB_TO_FILE.Courses> mapOfCourses = new HashMap();
    
    public CoursesDB_TO_FILE(Connection c, File f) throws SQLException, IOException{
        this.connection = c;
        this.file = f;
        this.init();
        this.writeToFile();
        //System.out.println(this.rowsOfInvigilators.toString());
    }
    
   private void writeToFile() throws IOException{
        StringBuilder build = new StringBuilder();
        for(String cour : this.mapOfCourses.keySet()){
            build.append(this.mapOfCourses.get(cour).getLevel());
            build.append("|");
            build.append(cour);
            build.append("|");
            build.append(this.mapOfCourses.get(cour).getCount());
            build.append("|");
            String depts = "";
            for(String mat: this.rowsOfStudents.keySet()){
                if(this.rowsOfStudents.get(mat).offerThisCourse(cour)){
                    if(!depts.contains(this.rowsOfStudents.get(mat).getProgramName()))
                        depts+=this.rowsOfStudents.get(mat).getProgramName()+"#";
                }
            }
            int cc = depts.length();
            if(cc > 0){
                depts = depts.substring(0, cc-1);
            }
            build.append(depts);
            build.append("\n");           
            //then all departments offering the course.           
        }
        build.append("-->END<--");
        try (FileWriter writerObj = new FileWriter(this.file)) {
            writerObj.write(build.toString());
            writerObj.flush();
        }
    }
    
    private void init() throws SQLException{
        this.statement = this.connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);      
        ResultSet result = this.statement.executeQuery("SELECT * FROM studentsinfo INNER JOIN programmes ON "
                + "studentsinfo.ProgrammeID=programmes.ProgrammeID INNER JOIN courseregistrations ON studentsinfo.MatricNumber="
                + "courseregistrations.MatricNumber ORDER BY studentsinfo.MatricNumber");
        while(result.next()){
            String matric = result.getString("studentsinfo.MatricNumber");
            String courseCode = result.getString("CourseCode");
            if(this.rowsOfStudents.containsKey(matric)){
                this.rowsOfStudents.get(matric).addCourse(courseCode);
            }
            else{
                this.rowsOfStudents.put(matric, new CoursesDB_TO_FILE.CourseRow(matric, result.getString("ProgrammeName"), 
                        result.getInt("Level")));
                this.rowsOfStudents.get(matric).addCourse(courseCode);
            }
            
            if(this.mapOfCourses.containsKey(courseCode)){
                this.mapOfCourses.get(courseCode).increament();
            }
            else{
                this.mapOfCourses.put(courseCode, new CoursesDB_TO_FILE.Courses(result.getInt("Level")));
            }
        }
    }
    
    private class Courses{
        private int count = 1;
        private int lev;
        
        public Courses(int l){
            this.lev = l;
        }
        
        public void increament(){
            this.count++;
        }
        
        public int getLevel(){
            return this.lev;
        }
        
        public int getCount(){
            return this.count;
        }
    }
    
    private class CourseRow{
        
        private String matric;
        private int level;
        private String programName;
        private List<String> listOfCourseCodes = new ArrayList<>();
        
        public CourseRow(String m, String p, int l){
            this.matric = m;
            this.programName = p;
            this.level = l;
        }
        
        public void addCourse(String code){
            this.listOfCourseCodes.add(code);
        }

        public String getMatric() {
            return matric;
        }

        public int getLevel() {
            return level;
        }

        public String getProgramName() {
            return programName;
        }

        public List<String> getListOfCourseCodes() {
            return listOfCourseCodes;
        }
        
        public boolean offerThisCourse(String course){
            if(this.listOfCourseCodes.contains(course)){
                return true;
            }
            else{
                return false;
            }
        }        
    }
}
