/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data_processor;

import data_processor.db_to_file.util.*;
import db.DatabaseConnectionInstance;
import java.sql.Connection;
import java.io.*;
import java.sql.SQLException;

/**
 *
 * @author MUSTAFA-OCMJD
 */
public class DataProcessorMain {
    
    public static void main(String []args){
        init(Modes.WRITE_TO_FILE);
    }
    
    
    private static void init(Modes m){
        switch(m){
            case WRITE_TO_FILE:
                DatabaseConnectionInstance dbConnect = new DatabaseConnectionInstance("localhost", "ttas");
                Connection connection = dbConnect.getConnection();
                try{
                    //new InvigilatorsDB_TO_FILE(connection, new File(new File("").getAbsolutePath()+"\\data\\invigilators.data"));
                    new CoursesDB_TO_FILE(connection, new File(new File("").getAbsolutePath()+"\\data\\courses.data")); 
                }
                catch(SQLException sE){
                    System.out.println("Error has to do with the Database : "+sE.getMessage());
                }
                catch(IOException iO){
                    System.out.println("Error has to do writing to the file : "+iO.getMessage());
                }
                break;
            default:
                break;
        }
    }
}
